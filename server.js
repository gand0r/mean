const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();

//APi file for interacting with MongoDB
const api = require('./server/routes/api');

// parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// Angular Dist ouput folder
app.use(express.static(path.join(__dirname, 'dist')));

// Api Location
app.use('/', api);

// Send all other request to the Angular app
app.get('*',(req , res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));;
});

// Set Port
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Running on localhost:${port}`));
